﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Accord.Audio;
using Accord.Audio.Formats;
using Accord.Audition;
using Accord.DataSets;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Math.Geometry;
using Accord.Statistics.Kernels;

namespace VoiceID.CLI
{
    class Sample
    {
        public Signal Input { get; set; }

        public int Output { get; set; }
    }

    class Recognizer
    {
        private BagOfAudioWords bagOfAudioWords;
        private MulticlassSupportVectorMachine<Linear> msvm;

        public void Learn(string trainingDataDirectory)
        {
            var trainingSamples = LoadTrainingSamples(trainingDataDirectory);

            var trainingInputSignals = trainingSamples
                .Select(x => x.Input)
                .ToArray();

            bagOfAudioWords = CreateBagOfAudioWords(trainingSamples);
            var trainInputs = bagOfAudioWords.Transform(trainingInputSignals);

            var teacher = new MulticlassSupportVectorLearning<Linear>()
            {
                Learner = (p) => new SequentialMinimalOptimization<Linear>()
                {
                    Complexity = 1.0
                }
            };

            msvm = teacher.Learn(trainInputs, trainingSamples.Select(x => x.Output).ToArray());
        }

        public int Decide(string filePath)
        {
            var signal = LoadSample(filePath).Input;
            var input = bagOfAudioWords.Transform(signal);

            return msvm.Decide(input);
        }

        private static BagOfAudioWords CreateBagOfAudioWords(List<Sample> samples)
        {
            var trainingInputSignals = samples
                .Select(x => x.Input)
                .ToArray();

            var bagOfAudioWords = BagOfAudioWords.Create(numberOfWords: 32);
            bagOfAudioWords.Learn(trainingInputSignals);

            return bagOfAudioWords;
        }

        private static List<Sample> LoadTrainingSamples(string directoryPath)
        {
            return Directory.GetFiles(directoryPath)
                .Select(LoadSample)
                .Where(x => x != null)
                .ToList();
        }

        private static Sample LoadSample(string filePath)
        {
            var label = int.Parse(Path.GetFileName(filePath).Split('_').First());

            return CreateSample(filePath, label);
        }

        public static Sample CreateSample(string filePath, int label)
        {
            var signal = ToSignal(filePath);

            if (signal == null)
                return null;

            return new Sample
            {
                Input = signal,
                Output = label
            };
        }

        private static Signal ToSignal(string filePath)
        {
            WaveDecoder decoder = null;
            Signal signal;

            try
            {
                decoder = new WaveDecoder(filePath);
                signal = decoder.Decode();
                decoder.Close();
            }
            catch (Exception e)
            {
                signal = null;
            }
            finally
            {
                decoder?.Close();
            }

            return signal;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var recognizer = new Recognizer();
            recognizer.Learn(@"C:\Users\Raven\Desktop\background_data");

            string command = string.Empty;

            while (!command.Contains("exit"))
            {
                Console.WriteLine("Type file path or 'exit':");
                command = Console.ReadLine();

                if (File.Exists(command))
                    Console.WriteLine($"Decision: {recognizer.Decide(command)}");
            }
        }
    }
}
