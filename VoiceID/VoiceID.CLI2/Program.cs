﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Accord.Audio;
using Accord.Audio.Formats;
using VoiceID.Core.Samples;
using VoiceID.Core.VoiceIdentification;

namespace VoiceID.CLI2
{
    public class Program
    {
        public VoiceIdentifier VoiceIdentifier { get; set; }

        public static void Main(string[] args)
        {
            var program = new Program();
            program.VoiceIdentifier = new VoiceIdentifier();

            program.Learn(args[0]);
            program.Predict(args[1]);

            Console.WriteLine("### DONE ###");
            Console.ReadLine();
        }

        public void Learn(string directoryPath)
        {
            var samples = ReadSamplesToLearn(directoryPath).ToList();

            VoiceIdentifier.LoadSamples(samples);
        }

        public void Predict(string predictPath)
        {
            var samples = LoadSamplesToPredict(predictPath);

            foreach (var sample in samples)
            {
                var output = VoiceIdentifier.Decide(sample);
                Console.WriteLine($"Output: {output}");
            }
        }

        private IEnumerable<Sample> ReadSamplesToLearn(string directoryPath)
        {
            var filesPaths = Directory.GetFiles(directoryPath)
                .Where(x => x.Contains("wav"));

            foreach (var filePath in filesPaths)
            {
                var signal = ReadSignal(filePath);
                var label = int.Parse(Path.GetFileName(filePath).Split('_')[0]);

                yield return new Sample
                {
                    Label = label,
                    Signal = signal
                };
            }
        }

        private List<Signal> LoadSamplesToPredict(string directoryPath)
        {
            var filesPaths = Directory.GetFiles(directoryPath);
            return filesPaths
                .Select(ReadSignal)
                .ToList();
        }

        private Signal ReadSignal(string filePath)
        {
            Signal signal;

            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                var waveDecoder = new WaveDecoder(fileStream);
                signal = waveDecoder.Decode();
            }

            return signal;
        }
    }
}
