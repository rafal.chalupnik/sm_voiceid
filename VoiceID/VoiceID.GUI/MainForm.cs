﻿using Accord.Audio;
using Accord.Audio.Formats;
using Accord.DirectSound;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using VoiceID.Core.Samples;
using VoiceID.Core.VoiceIdentification;

namespace VoiceID.GUI
{
    public partial class MainForm : Form
    {
        private AudioCaptureDevice audioCaptureDevice;
        private float[] current;
        private TimeSpan duration;
        private WaveEncoder encoder;
        private int frames;
        private int samples;
        private Stream stream;

        private int firstFreeLabel = 1;
        private Dictionary<int, string> labels = new Dictionary<int, string>()
        {
            {-1, "Unknown"}
        };
        private List<Sample> trainingSamples = new List<Sample>();

        private VoiceIdentifier voiceIdentifier = new VoiceIdentifier();

        public MainForm()
        {
            InitializeComponent();

            Wavechart.SimpleMode = true;
            Wavechart.AddWaveform("wave", Color.Green, 1, false);
        }

        private void RecordButton_Click(object sender, EventArgs e)
        {
            audioCaptureDevice = new AudioCaptureDevice
            {
                DesiredFrameSize = 4096,
                SampleRate = 22050,

                Format = SampleFormat.Format16Bit
            };

            audioCaptureDevice.NewFrame += AudioCaptureDevice_NewFrame;
            audioCaptureDevice.AudioSourceError += (o, args) => throw new Exception(args.Description);

            current = new float[audioCaptureDevice.DesiredFrameSize];

            stream = new MemoryStream();
            encoder = new WaveEncoder(stream);

            audioCaptureDevice.Start();
        }

        private void AudioCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // Save current frame
            eventArgs.Signal.CopyTo(current);

            // Update waveform
            UpdateWaveform(current, eventArgs.Signal.Length);

            // Save to memory
            encoder.Encode(eventArgs.Signal);

            // Update counters
            duration += eventArgs.Signal.Duration;
            samples += eventArgs.Signal.Samples;
            frames += eventArgs.Signal.Length;
        }

        private void UpdateWaveform(float[] samples, int length)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(() =>
                {
                    Wavechart.UpdateWaveform("wave", samples, length);
                }));
            }
            else
            {
                Wavechart.UpdateWaveform("wave", current, length);
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            if (audioCaptureDevice != null)
            {
                audioCaptureDevice.SignalToStop();
                audioCaptureDevice.WaitForStop();
            }

            Array.Clear(current, 0, current.Length);
            UpdateWaveform(current, current.Length);
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            var signal = GetActualSignal();

            var name = NameTextBox.Text;
            var description = DescriptionTextBox.Text;

            if (!labels.ContainsValue(name))
            {
                labels.Add(firstFreeLabel, name);
                firstFreeLabel++;
            }

            var sample = new Sample
            {
                Name = $"{name}_{description}",
                Label = 1,
                Signal = signal
            };

            SamplesListBox.Items.Add($"{name}_{description}");
            trainingSamples.Add(sample);
        }

        private void LearnButton_Click(object sender, EventArgs e)
        {
            var backgroundSamples = ReadBackgroundSamples();

            var allSamples = trainingSamples
                .Union(backgroundSamples)
                .ToList();

            voiceIdentifier.LoadSamples(allSamples);
        }

        private static List<Sample> ReadBackgroundSamples()
        {
            var filesPaths = Directory.GetFiles(@"C:\Users\Raven\Desktop\background_data");
            return filesPaths
                .Select(ReadSignal)
                .Select(signal => new Sample
                {
                    Signal = signal,
                    Label = 0
                }).ToList();
        }

        private static Signal ReadSignal(string filePath)
        {
            Signal signal;

            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                var waveDecoder = new WaveDecoder(fileStream);
                signal = waveDecoder.Decode();
            }

            return signal;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            var entryToRemove = SamplesListBox.Text;
            var removed = trainingSamples.RemoveAll(sample => sample.Name == entryToRemove);

            if (removed != 0)
            {
                SamplesListBox.Items.Remove(entryToRemove);
            }
        }

        private void IdentifyButton_Click(object sender, EventArgs e)
        {
            var signal = GetActualSignal();
            var decision = voiceIdentifier.Decide(signal);

            //var name = labels[decision];
            IdentificationResultLabel.Text = decision.ToString();
        }

        private Signal GetActualSignal()
        {
            stream.Seek(0, SeekOrigin.Begin);
            var waveDecoder = new WaveDecoder(stream);
            return waveDecoder.Decode();
        }
    }
}
