﻿namespace VoiceID.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SetupGroupBox = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.RegisterButton = new System.Windows.Forms.Button();
            this.LearnButton = new System.Windows.Forms.Button();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.SamplesListBox = new System.Windows.Forms.ListBox();
            this.RecognitionGroupBox = new System.Windows.Forms.GroupBox();
            this.IdentifyButton = new System.Windows.Forms.Button();
            this.IdentificationResultLabel = new System.Windows.Forms.Label();
            this.RecordingGroupBox = new System.Windows.Forms.GroupBox();
            this.Wavechart = new Accord.Controls.Wavechart();
            this.RecordButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.SetupGroupBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.RecognitionGroupBox.SuspendLayout();
            this.RecordingGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SetupGroupBox
            // 
            this.SetupGroupBox.Controls.Add(this.panel1);
            this.SetupGroupBox.Controls.Add(this.SamplesListBox);
            this.SetupGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SetupGroupBox.Location = new System.Drawing.Point(0, 0);
            this.SetupGroupBox.Name = "SetupGroupBox";
            this.SetupGroupBox.Size = new System.Drawing.Size(710, 534);
            this.SetupGroupBox.TabIndex = 0;
            this.SetupGroupBox.TabStop = false;
            this.SetupGroupBox.Text = "Setup";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DescriptionTextBox);
            this.panel1.Controls.Add(this.DescriptionLabel);
            this.panel1.Controls.Add(this.RegisterButton);
            this.panel1.Controls.Add(this.LearnButton);
            this.panel1.Controls.Add(this.NameTextBox);
            this.panel1.Controls.Add(this.DeleteButton);
            this.panel1.Controls.Add(this.NameLabel);
            this.panel1.Location = new System.Drawing.Point(457, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(247, 266);
            this.panel1.TabIndex = 8;
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.DescriptionTextBox.Location = new System.Drawing.Point(0, 46);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(247, 20);
            this.DescriptionTextBox.TabIndex = 4;
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DescriptionLabel.Location = new System.Drawing.Point(0, 33);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.DescriptionLabel.TabIndex = 3;
            this.DescriptionLabel.Text = "Description:";
            // 
            // RegisterButton
            // 
            this.RegisterButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.RegisterButton.Location = new System.Drawing.Point(0, 197);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(247, 23);
            this.RegisterButton.TabIndex = 7;
            this.RegisterButton.Text = "Register";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // LearnButton
            // 
            this.LearnButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LearnButton.Location = new System.Drawing.Point(0, 220);
            this.LearnButton.Name = "LearnButton";
            this.LearnButton.Size = new System.Drawing.Size(247, 23);
            this.LearnButton.TabIndex = 6;
            this.LearnButton.Text = "Learn";
            this.LearnButton.UseVisualStyleBackColor = true;
            this.LearnButton.Click += new System.EventHandler(this.LearnButton_Click);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.NameTextBox.Location = new System.Drawing.Point(0, 13);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(247, 20);
            this.NameTextBox.TabIndex = 2;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DeleteButton.Location = new System.Drawing.Point(0, 243);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(247, 23);
            this.DeleteButton.TabIndex = 5;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.NameLabel.Location = new System.Drawing.Point(0, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(38, 13);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Name:";
            // 
            // SamplesListBox
            // 
            this.SamplesListBox.FormattingEnabled = true;
            this.SamplesListBox.Location = new System.Drawing.Point(12, 19);
            this.SamplesListBox.Name = "SamplesListBox";
            this.SamplesListBox.Size = new System.Drawing.Size(439, 264);
            this.SamplesListBox.TabIndex = 0;
            // 
            // RecognitionGroupBox
            // 
            this.RecognitionGroupBox.Controls.Add(this.IdentifyButton);
            this.RecognitionGroupBox.Controls.Add(this.IdentificationResultLabel);
            this.RecognitionGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.RecognitionGroupBox.Location = new System.Drawing.Point(0, 434);
            this.RecognitionGroupBox.Name = "RecognitionGroupBox";
            this.RecognitionGroupBox.Size = new System.Drawing.Size(710, 100);
            this.RecognitionGroupBox.TabIndex = 1;
            this.RecognitionGroupBox.TabStop = false;
            this.RecognitionGroupBox.Text = "Recognition";
            // 
            // IdentifyButton
            // 
            this.IdentifyButton.Location = new System.Drawing.Point(402, 45);
            this.IdentifyButton.Name = "IdentifyButton";
            this.IdentifyButton.Size = new System.Drawing.Size(75, 23);
            this.IdentifyButton.TabIndex = 1;
            this.IdentifyButton.Text = "Identify";
            this.IdentifyButton.UseVisualStyleBackColor = true;
            this.IdentifyButton.Click += new System.EventHandler(this.IdentifyButton_Click);
            // 
            // IdentificationResultLabel
            // 
            this.IdentificationResultLabel.AutoSize = true;
            this.IdentificationResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IdentificationResultLabel.Location = new System.Drawing.Point(177, 43);
            this.IdentificationResultLabel.Name = "IdentificationResultLabel";
            this.IdentificationResultLabel.Size = new System.Drawing.Size(192, 25);
            this.IdentificationResultLabel.TabIndex = 0;
            this.IdentificationResultLabel.Text = "Identification result";
            // 
            // RecordingGroupBox
            // 
            this.RecordingGroupBox.Controls.Add(this.Wavechart);
            this.RecordingGroupBox.Controls.Add(this.RecordButton);
            this.RecordingGroupBox.Controls.Add(this.StopButton);
            this.RecordingGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.RecordingGroupBox.Location = new System.Drawing.Point(0, 291);
            this.RecordingGroupBox.Name = "RecordingGroupBox";
            this.RecordingGroupBox.Size = new System.Drawing.Size(710, 143);
            this.RecordingGroupBox.TabIndex = 2;
            this.RecordingGroupBox.TabStop = false;
            this.RecordingGroupBox.Text = "Recording";
            // 
            // Wavechart
            // 
            this.Wavechart.BackColor = System.Drawing.Color.Black;
            this.Wavechart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Wavechart.Location = new System.Drawing.Point(3, 16);
            this.Wavechart.Name = "Wavechart";
            this.Wavechart.RangeX = ((Accord.DoubleRange)(resources.GetObject("Wavechart.RangeX")));
            this.Wavechart.RangeY = ((Accord.DoubleRange)(resources.GetObject("Wavechart.RangeY")));
            this.Wavechart.SimpleMode = false;
            this.Wavechart.Size = new System.Drawing.Size(554, 124);
            this.Wavechart.TabIndex = 2;
            this.Wavechart.Text = "Wavechart";
            // 
            // RecordButton
            // 
            this.RecordButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.RecordButton.Location = new System.Drawing.Point(557, 16);
            this.RecordButton.Name = "RecordButton";
            this.RecordButton.Size = new System.Drawing.Size(75, 124);
            this.RecordButton.TabIndex = 0;
            this.RecordButton.Text = "Record";
            this.RecordButton.UseVisualStyleBackColor = true;
            this.RecordButton.Click += new System.EventHandler(this.RecordButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.StopButton.Location = new System.Drawing.Point(632, 16);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(75, 124);
            this.StopButton.TabIndex = 1;
            this.StopButton.Text = "Stop";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 534);
            this.Controls.Add(this.RecordingGroupBox);
            this.Controls.Add(this.RecognitionGroupBox);
            this.Controls.Add(this.SetupGroupBox);
            this.Name = "MainForm";
            this.Text = "VoiceID";
            this.SetupGroupBox.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.RecognitionGroupBox.ResumeLayout(false);
            this.RecognitionGroupBox.PerformLayout();
            this.RecordingGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox SetupGroupBox;
        private System.Windows.Forms.GroupBox RecognitionGroupBox;
        private System.Windows.Forms.GroupBox RecordingGroupBox;
        private System.Windows.Forms.Button RecordButton;
        private System.Windows.Forms.Button StopButton;
        private Accord.Controls.Wavechart Wavechart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Button RegisterButton;
        private System.Windows.Forms.Button LearnButton;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.ListBox SamplesListBox;
        private System.Windows.Forms.Button IdentifyButton;
        private System.Windows.Forms.Label IdentificationResultLabel;
    }
}

