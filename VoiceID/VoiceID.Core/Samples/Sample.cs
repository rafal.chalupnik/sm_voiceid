﻿using Accord.Audio;

namespace VoiceID.Core.Samples
{
    public class Sample
    {
        public string Name { get; set; }

        public int Label { get; set; }

        public Signal Signal { get; set; }
    }
}