﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Accord.Audio;
using Accord.Audio.Formats;
using VoiceID.Core.Samples;

namespace VoiceID.Core.FileLoading
{
    public class FileLoader : IFileLoader
    {
        public Signal LoadFile(string filePath)
        {
            return ReadSignal(filePath);
        }

        public List<Sample> LoadTrainingFiles(string directoryPath)
        {
            return Directory.GetFiles(directoryPath)
                .Select(LoadSample)
                .Where(x => x != null)
                .ToList();
        }

        private static Sample LoadSample(string filePath)
        {
            var label = int.Parse(Path.GetFileName(filePath).Split('_').First());

            return CreateSample(filePath, label);
        }

        public static Sample CreateSample(string filePath, int label)
        {
            var signal = ReadSignal(filePath);

            if (signal == null)
                return null;

            return new Sample
            {
                Signal = signal,
                Label = label
            };
        }

        private static Signal ReadSignal(string filePath)
        {
            WaveDecoder decoder = null;
            Signal signal;

            try
            {
                decoder = new WaveDecoder(filePath);
                signal = decoder.Decode();
                decoder.Close();
            }
            catch (Exception e)
            {
                signal = null;
            }
            finally
            {
                decoder?.Close();
            }

            return signal;
        }
    }
}