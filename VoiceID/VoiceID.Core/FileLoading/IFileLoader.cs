﻿using System.Collections.Generic;
using Accord.Audio;
using VoiceID.Core.Samples;

namespace VoiceID.Core.FileLoading
{
    public interface IFileLoader
    {
        Signal LoadFile(string filePath);

        List<Sample> LoadTrainingFiles(string directoryPath);
    }
}