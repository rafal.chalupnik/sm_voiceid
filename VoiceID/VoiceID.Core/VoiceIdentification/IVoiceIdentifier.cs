﻿using System.Collections.Generic;
using Accord.Audio;
using VoiceID.Core.Samples;

namespace VoiceID.Core.VoiceIdentification
{
    public interface IVoiceIdentifier
    {
        int Decide(Signal signal);

        void Export(string filePath);

        void Import(string directoryPath);

        void LoadSamples(List<Sample> samples);
    }
}