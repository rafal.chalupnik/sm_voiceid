﻿using Accord.Audio;
using Accord.Audition;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using System.Collections.Generic;
using System.Linq;
using Accord.IO;
using VoiceID.Core.Samples;

namespace VoiceID.Core.VoiceIdentification
{
    public class VoiceIdentifier : IVoiceIdentifier
    {
        private const double CertaintyTreshold = 0.5;
        private const int NumberOfWords = 32;

        private BagOfAudioWords bagOfAudioWords;
        private MulticlassSupportVectorMachine<Linear> multiclassSupportVectorMachine;

        public int Decide(Signal signal)
        {
            var input = bagOfAudioWords.Transform(signal);
            var probabilities = multiclassSupportVectorMachine.Probabilities(input);

            var maxScore = double.MinValue;
            var bestLabel = -1;

            for (var i = 0; i < probabilities.Length; i++)
            {
                if (probabilities[i] > maxScore)
                {
                    maxScore = probabilities[i];
                    bestLabel = i;
                }
            }

            return maxScore >= CertaintyTreshold
                ? bestLabel
                : -1;

            //return bestLabel;
        }

        public void Export(string directoryPath)
        {
            bagOfAudioWords.Save($@"{directoryPath}\BagOfAudioWords.data");
            multiclassSupportVectorMachine.Save($@"{directoryPath}\MulticlassSupportVectorMachine.data");
        }

        public void Import(string directoryPath)
        {
            bagOfAudioWords = Serializer.Load<BagOfAudioWords>($@"{directoryPath}\BagOfAudioWords.data");
            multiclassSupportVectorMachine =
                Serializer.Load<MulticlassSupportVectorMachine<Linear>>($@"{directoryPath}\MulticlassSupportVectorMachine.data");
        }

        public void LoadSamples(List<Sample> samples)
        {
            var signals = samples
                .Select(x => x.Signal)
                .ToArray();
            var labels = samples
                .Select(x => (double) x.Label)
                .ToArray();

            bagOfAudioWords = CreateBagOfAudioWords(signals);
            multiclassSupportVectorMachine = CreateMulticlassSupportVectorMachine(samples);
        }

        private static BagOfAudioWords CreateBagOfAudioWords(Signal[] signals)
        {
            var bagOfAudioWords = BagOfAudioWords.Create(NumberOfWords);
            bagOfAudioWords.Learn(signals);
            return bagOfAudioWords;
        }

        private MulticlassSupportVectorMachine<Linear> CreateMulticlassSupportVectorMachine(
            IReadOnlyCollection<Sample> samples)
        {
            var teacher = new MulticlassSupportVectorLearning<Linear>
            {
                Learner = p => new SequentialMinimalOptimization<Linear>
                {
                    Complexity = 0.01
                }
            };

            var signals = samples
                .Select(x => x.Signal)
                .ToArray();

            var trainInputs = bagOfAudioWords.Transform(signals);
            var trainOutputs = samples
                .Select(x => x.Label)
                .ToArray();

            return teacher.Learn(trainInputs, trainOutputs);
        }
    }
}